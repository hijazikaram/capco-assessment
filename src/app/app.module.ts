import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { PaginationComponent } from "./pagination/pagination.component";
import { BeautifyCamelcasePipe } from "./beautify-camelcase-pipe.pipe";
import { UserService } from "./user.service";

@NgModule({
  declarations: [AppComponent, PaginationComponent, BeautifyCamelcasePipe],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [UserService],
  bootstrap: [AppComponent]
})
export class AppModule {}
