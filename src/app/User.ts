export default class User {
  name: String;
  phone: String;
  email: String;
  org_num: String;
  address_1: String;
  city: String;
  zip: String;
  geo: String;
  pan: String;
  pin: String;
  status: String;
  fee: String;
  guid: String;
  date_entry: String;
  date_exit: String;
  date_first: String;
  date_recent: String;
  url: String;
  id: number;
  company: string;
}
