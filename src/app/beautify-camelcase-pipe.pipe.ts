import { Pipe, PipeTransform } from "@angular/core";

/**
 * @Pipe
 * Using this to capitalize fist letter of string and adding space everytime there is a underscore.
 *
 * @return {string} The Prettify CamelCase pipe
 */
@Pipe({
  name: "beautifyCamelCase"
})
export class BeautifyCamelcasePipe implements PipeTransform {
  public transform(value: string): string {
    // replace uppercase with space
    value = value.replace(/_/g, " ").trim();
    // capitalize the first letter of the string
    return value.charAt(0).toUpperCase() + value.slice(1);
  }
}
