import { TestBed } from "@angular/core/testing";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { UserService } from "./user.service";
import User from "./User";

describe("UserService", () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [UserService]
    })
  );

  it("should be created", () => {
    const service: UserService = TestBed.get(UserService);
    expect(service).toBeTruthy();
  });

  it("user should be updated", () => {
    const user: User = {
      name: "Nell D. Michael",
      phone: "602-1033",
      email: "hendrerit.id.ante@placeratvelit.ca",
      company: "Praesent Eu LLP",
      date_entry: "2017-07-30 23:27:39",
      org_num: "907369 2973",
      address_1: "P.O. Box 916, 8584 Vestibulum St.",
      city: "Vitry-sur-Seine",
      zip: "2353",
      geo: "60.77971, 7.98874",
      pan: "4532992507580",
      pin: "7086",
      id: 1,
      status: "read",
      fee: "$60.99",
      guid: "48653E36-987F-48EC-7382-7F009FF34628",
      date_exit: "2018-11-14 12:37:54",
      date_first: "2018-05-20 01:07:05",
      date_recent: "2019-04-06 23:28:25",
      url: "https://capco.com/"
    };
    const service: UserService = TestBed.get(UserService);
    service
      .updateUser(user)
      .subscribe((response: { id: number; status: string }) => {
        expect(response.id).toEqual(user.id);
        expect(response.status).toEqual(user.status);
      });
  });
});
