import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import User from "./User";

@Injectable({
  providedIn: "root"
})
export class UserService {
  public url: string = "https://jsonplaceholder.typicode.com/posts/";
  public loggedInUser: User;
  constructor(private http: HttpClient) {}

  public updateUser(user: User): Observable<Object> {
    return this.http.post(this.url, { id: user.id, status: user.status });
  }
}
