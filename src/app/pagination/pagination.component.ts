import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import User from "../User";
@Component({
  selector: "app-pagination",
  templateUrl: "./pagination.component.html",
  styleUrls: ["./pagination.component.css"]
})
export class PaginationComponent implements OnInit {
  @Input() page: number; // page number to be displayed initially
  @Input() count: number; //  per page count
  @Input() data: User[]; // total data received here
  @Input() dataTobeDisplayed: User[];

  @Output() pageDataToDisplay = new EventEmitter<any>(); // per page data will be emitted to parent component

  public perPage: number = 20;
  public selectedPage: number = 1;

  constructor() {}

  public ngOnInit() {
    if (this.data && this.data.length > 0) {
      this.sendData(); // to send per page data onload
    }
  }

  // to send per page data
  public sendData() {
    this.dataTobeDisplayed = this.data.slice(
      (this.selectedPage - 1) * this.perPage,
      this.selectedPage * this.perPage
    );
    this.pageDataToDisplay.emit(this.dataTobeDisplayed);
  }

  // to send per page data onload
  public onPageSelect(i: number) {
    this.selectedPage = i;
    this.sendData();
  }

  // to change per page count
  public onLimitChange() {
    if (!this.getPages().includes(this.selectedPage)) {
      this.selectedPage = 1;
    }
    this.sendData();
  }

  public onPrev(): void {
    this.selectedPage = this.selectedPage - 1;
    this.sendData();
  }

  public onNext(): void {
    this.selectedPage = this.selectedPage + 1;
    this.sendData();
  }

  public lastPage(): boolean {
    return this.perPage * this.page > this.count;
  }

  public getPages(): number[] {
    let pagesArr: number[] = [];
    for (let i: any = 0; i < this.data.length / this.perPage; i++) {
      pagesArr.push(i + 1);
    }
    return pagesArr;
  }
}
