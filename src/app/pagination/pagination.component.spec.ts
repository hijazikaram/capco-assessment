import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { PaginationComponent } from "./pagination.component";
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";

describe("PaginationComponent", () => {
  let component: PaginationComponent;
  let fixture: ComponentFixture<PaginationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, HttpClientModule],
      declarations: [PaginationComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it(`should have perPage set to 20 by default`, () => {
    const fixture = TestBed.createComponent(PaginationComponent);
    const pagination = fixture.debugElement.componentInstance;
    expect(pagination.perPage).toEqual(20);
  });

  it(`should have selectedPage set to 1 by default`, () => {
    const fixture = TestBed.createComponent(PaginationComponent);
    const pagination = fixture.debugElement.componentInstance;
    expect(pagination.selectedPage).toEqual(1);
  });
});
