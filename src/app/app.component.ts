import { Component } from "@angular/core";
import User from "./User";
import sampleData from "./data.json";
import { UserService } from "./user.service";
@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  public users: User[] = sampleData; // total users data will be stored here
  public page: number = 1; // page to be displayed initially
  public limit: number = 20; // initial no of rows to be diplayed per each page
  public columns: any = []; // headers of users data
  public pageData: User[] = []; // data to be displayed for each page
  public noData: string; // show message when getting no data

  // user service will provide all users data
  constructor(private service: UserService) {}

  public ngOnInit() {
    if (this.users && this.users.length > 0) {
      this.noData = null;
      // this will add all headers of users data from json file dynamically
      for (let x in this.users[0]) {
        this.columns.push(x);
      }
    } else {
      // setting message to show the user
      this.noData = "There is no data to be shown.";
    }
  }
  // to receive data to be displayed per page from pagination component
  public pageDataToDisplay(data: User[]) {
    this.pageData = data;
  }

  // to make api call on clicking submit button
  public onSubmit(selectedUser: User) {
    this.service.updateUser(selectedUser).subscribe(
      () => {
        alert("success");
      },
      () => {
        alert("failed");
      }
    );
  }
}
