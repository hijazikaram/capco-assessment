# Angular Developer Assesment
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.2.3.
Submission Author: Karam Hijazi, hijazikaram@gmail.com

## Technologies:  
Angular 8.2.3 (Angular CLI), Karma/Jasmine, CSS

## Project Requirements
The objective is to implement a TABLE component according to the specifications outlined below, using the listed technologies only – please do not use 3rd party table components such as those included with Bootstrap, Angular Material, etc.

### High-level Specifications:
-	The component should display Sample Data in a table
-	User should be able to select how many rows are displayed in the table
-	Table should be paginated if not all rows are displayed on the screen based on the user’s selection
-	Pagination options should be displayed in the table footer
-	Column names should be displayed in the table header
-	Entire table, table header and table footer should always be displayed on the screen while scrolling
-	If number of rows exceeds the size of the table body, a vertical scrollbar should be displayed within the table body – only table body shall scroll vertically, table header and footer shall remain as is
-	If number of columns exceed the size of the table body, a horizontal scrollbar should be displayed within the table body – only table body and table header shall scroll to reveal the additional columns, table footer shall remain as is
-	Each row should contain a button which shall submit the row ID and row status to /api/submit as a POST request – You are not expected to create the POST endpoint, but you can mock one if you like

### Technologies
-	Angular 4+ (solution completed in Angular 8.2.3)
-	Angular CLI
-	Karma / Jasmine
-	Sass/Scss (optional)

## Solution Notes
The container for this table is scrollable horzontally.
The single, default route is "/" for simpicity.
Sample Data is imported locally via the JSON file.
Pagination options are: 10, 20 (default), 50, 100.

Paginator accepts the following attribute/inputs:
[data] - accepts all the users.
[page] - page to be displayed initially.
[count] - accepts per page count.
(pageDataToDisplay) - per page data will be emitted to parent component.

BeautifyCamelcasePipe - Takes in a string, replaces the underscore with space and capatlizes the first letter of the string.

Recommended build:  ng build --aot=true --buildOptimizer=true --prod=true
*Depending on your hosting location, you may require a --base-href

## Additional Notes
Sample JSON data was provided.

## Thanks for giving me a chance!
It was a pleasant task and I understand how long you are going to waste assessing this endeavor. If you have any concerns or feedback, feel entitled to contact you.

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
Run `npm start` for a dev server with AOT. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build
Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests
Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running Prettify
Run `npm prettify` to format all the files [Prettier](https://prettier.io).

